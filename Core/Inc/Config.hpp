/*
 * Config.hpp
 *
 *  Created on: Jul 22, 2023
 *      Author: kryst
 */

#ifndef INC_CONFIG_HPP_
#define INC_CONFIG_HPP_


/* MOSFET DRIVER DRV83053 */
#define DRV_CFG_HS_GATE 0x344 				//HS Gate Drive Control (Address = 0x5)
#define DRV_CFG_LS_GATE 0x344 				//LS Gate Drive Control (Address = 0x6)
#define DRV_CFG_GATE 0x296 					//Gate Drive Control (Address = 0x7)
#define DRV_CFG_IC_OPERATION 0x420			//IC Operation (Address = 0x9)
#define DRV_CFG_SHUNT 0x0					//Shunt Amplifier Control (Address = 0xA)
#define DRV_CFG_VOLTAGE_REG 0x10A			//Voltage Regulator Control (Address = 0xB)
#define DRV_CFG_VDS_SENSE 0x10				//VDS Sense Control (Address = 0xC)
#define DRV_CFG_SPI_TIMEOUT 10
#define DRV_CFG_SPI_SIZE 1	// we send 1 frame that consist of 16 bits, spi is configured to sending 16 bit frame




#endif /* INC_CONFIG_HPP_ */
