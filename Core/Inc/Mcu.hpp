/*
 * Mcu.hpp
 *
 *  Created on: Jul 22, 2023
 *      Author: kryst
 */

#ifndef INC_MCU_HPP_
#define INC_MCU_HPP_

#include "main.h"

void mcu_delay_ms(uint32_t time_delay);

void mcu_spi_transmit_and_receive(SPI_HandleTypeDef* hspi, uint8_t* frame, uint8_t* answer, uint16_t size, uint32_t timeout);

enum class Pinout {
	DRV_SPI_SS,
	DRV_WAKE,
	DRV_ENABLE,
	LD2,
	LD3
};

void mcu_write_gpio(Pinout pin, bool state) {
	switch(pin) {
	case: DRV_SPI
		HAL_GPIO_Write....
	}
}



#endif /* INC_MCU_HPP_ */
