/*
 * Driver8305.hpp
 *
 *  Created on: Jul 22, 2023
 *      Author: kryst
 */

#ifndef INC_DRIVER8305_HPP_
#define INC_DRIVER8305_HPP_

#include "Mcu.hpp"
#include "main.h"
#include "Config.hpp"
#include <cstring>

namespace bldc {

class Driver8305 {

public:

	struct frame_bitfield {
		uint16_t data :11;
		uint16_t addres :4;
		uint16_t read_write :1;
	}typedef frame_bitfield;

	union driver_frame {
		frame_bitfield bitfield;
		uint8_t data_tab[2];
		uint16_t frame;
	}typedef driver_frame;

	enum class register_map : uint8_t {
		WARNINGS_AND_WATCHDOG_RESET = 0x1, 	//read only
		OV_VDS_FAULTS = 0x2, 				//read only
		IC_FAULTS = 0x3,					//read only
		VGS_FAULTS = 0x04,					//read only
		HS_GATE = 0x5,						//read and write
		LS_GATE = 0x6,						//read and write
		GATE = 0x7,							//read and write
		RESERVED = 0x8,
		IC_OPERATION = 0x9,					//read and write
		SHUNT = 0xA,						//read and write
		VOLTAGE_REG = 0xB,					//read and write
		VDS_SENSE = 0xC						//read and write
	};

	/* registers structures - faults and warnings*/
		struct reg_warnings {
			uint32_t otw : 1;
			uint32_t temp_flag3 : 1;
			uint32_t temp_flag2 : 1;
			uint32_t temp_flag1 : 1;
			uint32_t vcph_uvfl : 1;
			uint32_t vds_status : 1;
			uint32_t pvdd_ovfl : 1;
			uint32_t pvdd_uvfl : 1;
			uint32_t temp_flag4 : 1;
			uint32_t rsvd : 1;
			uint32_t fault : 1;
			uint32_t empty : 21;
		} typedef reg_warnings;

		struct reg_ovvds_faults {
			uint32_t sns_a_ocp : 1;
			uint32_t sns_b_ocp : 1;
			uint32_t sns_c_ocp : 1;
			uint32_t rsvd : 2;
			uint32_t vds_lc : 1;
			uint32_t vds_hc : 1;
			uint32_t vds_lb : 1;
			uint32_t vds_hb : 1;
			uint32_t vds_la : 1;
			uint32_t vds_ha : 1;
			uint32_t empty : 21;
		} typedef reg_ovvds_faults;

		struct reg_ic_faults {
			uint32_t vcph_ovlo_abs : 1;
			uint32_t vcph_ovlo : 1;
			uint32_t vcph_uvlo2 : 1;
			uint32_t rsvd1 : 1;
			uint32_t vcp_lsd_uvlo2 : 1;
			uint32_t avdd_uvlo : 1;
			uint32_t vreg_uv : 1;
			uint32_t rsvd2 : 1;
			uint32_t otsd : 1;
			uint32_t wd_fault : 1;
			uint32_t pvdd_uvlo2 : 1;
			uint32_t empty : 21;
		} typedef reg_ic_faults;

		struct reg_vgs_faults {
			uint32_t rsvd : 5;
			uint32_t vgs_lc : 1;
			uint32_t vgs_hc : 1;
			uint32_t vgs_lb : 1;
			uint32_t vgs_hb : 1;
			uint32_t vgs_la : 1;
			uint32_t vgs_ha : 1;
			uint32_t empty : 21;
		} typedef reg_vgs_faults;

	Driver8305(SPI_HandleTypeDef* hspi);
	virtual ~Driver8305();
	void read_register(register_map addres);
	void init_register();
	void read_diag_registers();
	SPI_HandleTypeDef* hspi;

	driver_frame outgoing_frame;
	driver_frame incoming_frame;
private:
	void write_register(register_map addres, uint16_t value);
	void interprete_frame(register_map addres);
	bool verify_answer(register_map addres);

	/* registers, warnings and faults */
	reg_warnings warnings;
	reg_ovvds_faults ovvds_faults;
	reg_ic_faults ic_faults;
	reg_vgs_faults vgs_faults;


};


}

#endif /* INC_DRIVER8305_HPP_ */
