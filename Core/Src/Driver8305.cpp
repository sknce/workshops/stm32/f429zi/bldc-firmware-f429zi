/*
 * Driver8305.cpp
 *
 *  Created on: Jul 22, 2023
 *      Author: kryst
 */

#include <Driver8305.hpp>

bldc::Driver8305::Driver8305(SPI_HandleTypeDef* hspi) : hspi(hspi) {
	// TODO Auto-generated constructor stub
}

bldc::Driver8305::~Driver8305() {
	// TODO Auto-generated destructor stub
}

void bldc::Driver8305::read_register(register_map addres) {

	outgoing_frame.bitfield.read_write = 1; //1 when we read, 0 when we write
	outgoing_frame.bitfield.addres = (uint8_t)addres;
	outgoing_frame.bitfield.data = 0; // it doesn't matter when we read
	mcu_spi_transmit_and_receive(hspi, outgoing_frame.data_tab, incoming_frame.data_tab, 1, DRV_CFG_SPI_TIMEOUT);
	interprete_frame(addres);
}

void bldc::Driver8305::write_register(register_map addres, uint16_t value) {

	outgoing_frame.bitfield.read_write = 0; //1 when we read, 0 when we write
	outgoing_frame.bitfield.addres = (uint8_t)addres;
	outgoing_frame.bitfield.data = value;
	mcu_spi_transmit_and_receive(hspi, outgoing_frame.data_tab, incoming_frame.data_tab, 1, DRV_CFG_SPI_TIMEOUT);
	verify_answer(addres);
}

void bldc::Driver8305::init_register() {


	write_register(register_map::HS_GATE, 	(uint16_t)DRV_CFG_HS_GATE);
	write_register(register_map::LS_GATE, 	(uint16_t)DRV_CFG_LS_GATE);
	write_register(register_map::GATE, 		(uint16_t)DRV_CFG_GATE);
//	write_register(hspi, register_map::IC_OPERATION, 	drv_config.ic_operation);
//	write_register(hspi, register_map::SHUNT, 		drv_config.shunt);
//	write_register(hspi, register_map::VOLTAGE_REG, 		drv_config.voltage_reg);
//	write_register(hspi, register_map::VDS_SENSE, 		drv_config.vds_sense);
}

void bldc::Driver8305::read_diag_registers() {
	// read registers that contains warning and faults flags
	read_register(register_map::WARNINGS_AND_WATCHDOG_RESET);
	read_register(register_map::OV_VDS_FAULTS);
	read_register(register_map::IC_FAULTS);
	read_register(register_map::VGS_FAULTS);
}

void bldc::Driver8305::interprete_frame(register_map addres) {
	switch((register_map)addres) {
		case register_map::WARNINGS_AND_WATCHDOG_RESET:
			memcpy(&warnings, &incoming_frame, 2);
			break;
		case register_map::OV_VDS_FAULTS:
			memcpy(&ovvds_faults, &incoming_frame, 2);
			break;
		case register_map::IC_FAULTS:
			memcpy(&ic_faults, &incoming_frame, 2);
			break;
		case register_map::VGS_FAULTS:
			memcpy(&vgs_faults, &incoming_frame, 2);
			break;
		case register_map::HS_GATE:
			break;
		case register_map::LS_GATE:
			break;
		case register_map::GATE:
			break;
		case register_map::RESERVED:
			break;
		case register_map::IC_OPERATION:
			break;
		case register_map::SHUNT:
			break;
		case register_map::VOLTAGE_REG:
			break;
		case register_map::VDS_SENSE:
			break;
		default:
			break;
		}
}

bool bldc::Driver8305::verify_answer(register_map addres) {
	bool err = 0;

	switch ((register_map) addres) {
	case register_map::WARNINGS_AND_WATCHDOG_RESET:
		break;
	case register_map::OV_VDS_FAULTS:
		break;
	case register_map::IC_FAULTS:
		break;
	case register_map::VGS_FAULTS:
		break;
	case register_map::HS_GATE:
		err = (outgoing_frame.bitfield.data & 0x3FF)
				!= (incoming_frame.bitfield.data & 0x3FF); // bits 0 to 9 are possible to write
		break;
	case register_map::LS_GATE:
		err = (outgoing_frame.bitfield.data & 0x3FF)
				!= (incoming_frame.bitfield.data & 0x3FF); // bits 0 to 9 are possible to write
		break;
	case register_map::GATE:
		err = (outgoing_frame.bitfield.data & 0x3FF)
				!= (incoming_frame.bitfield.data & 0x3FF); // bits 0 to 9 are possible to write
		break;
	case register_map::RESERVED:
		break;
	case register_map::IC_OPERATION:
		err = (outgoing_frame.bitfield.data & 0x7FF)
				!= (incoming_frame.bitfield.data & 0x7FF); // bits 0 to 10 are possible to write
		break;
	case register_map::SHUNT:
		err = (outgoing_frame.bitfield.data & 0x7FF)
				!= (incoming_frame.bitfield.data & 0x7FF); // bits 0 to 10 are possible to write
		break;
	case register_map::VOLTAGE_REG:
		err = (outgoing_frame.bitfield.data & 0x31F)
				!= (incoming_frame.bitfield.data & 0x31F); // bits 0 to 4 and 8 do 9 are possible to write
		break;
	case register_map::VDS_SENSE:
		err = (outgoing_frame.bitfield.data & 0xFF)
				!= (incoming_frame.bitfield.data & 0xFF); // bits 0 to 7 are possible to write
		break;
	default:
		err = 1;
		break;
	}

	if (err) {
		int test = 0;
	}

	return err;
}
