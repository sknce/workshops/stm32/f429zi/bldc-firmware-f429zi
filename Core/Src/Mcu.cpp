/*
 * Mcu.cpp
 *
 *  Created on: Jul 22, 2023
 *      Author: kryst
 */

#include "Mcu.hpp"

void mcu_delay_ms(uint32_t time_delay) {
	HAL_Delay(time_delay);
}

void mcu_spi_transmit_and_receive(SPI_HandleTypeDef* hspi, uint8_t* frame, uint8_t* answer, uint16_t size, uint32_t timeout) {

	HAL_GPIO_WritePin(DRV_SPI_NSS_GPIO_Port, DRV_SPI_NSS_Pin, GPIO_PIN_RESET); 	// chip select
	HAL_SPI_TransmitReceive(hspi, frame, answer, size, timeout);				// sending 16-bit frame
	HAL_GPIO_WritePin(DRV_SPI_NSS_GPIO_Port, DRV_SPI_NSS_Pin, GPIO_PIN_SET); 	// chip select
	mcu_delay_ms(1); // CHIP SELECT should be high 500ns or more between two frames
}

// TODO: blablabla

// Funkcja mcu_spi_transmit_and_receive ma przyjąć wskaźnik na tablicę uint8_t oraz wysłać te dane za pomocą HAL_SPI_TransmitReceive
// Odebrane dane mają być wpisane pod wskazany adres lub mają być zwrócone.
// parametryzacja size i timeout

//DRV_SPI_NSS_GPIO_Port, DRV_SPI_NSS_Pin
// HAL_GPIO_WritePin(DRV_SPI_NSS_GPIO_Port, DRV_SPI_NSS_Pin, GPIO_PIN_RESET);
// HAL_GPIO_WritePin(DRV_SPI_NSS_GPIO_Port, DRV_SPI_NSS_Pin, GPIO_PIN_SET);
